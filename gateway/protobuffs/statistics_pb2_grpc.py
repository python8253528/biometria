# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from protobuffs import statistics_pb2 as statistics__pb2


class StatisticServiceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.CreateOperation = channel.unary_unary(
                '/protobuffs.StatisticService/CreateOperation',
                request_serializer=statistics__pb2.StatisticRequest.SerializeToString,
                response_deserializer=statistics__pb2.StatisticDetailOperationResponse.FromString,
                )
        self.CreateTransaction = channel.unary_unary(
                '/protobuffs.StatisticService/CreateTransaction',
                request_serializer=statistics__pb2.StatisticTransactionCreateRequest.SerializeToString,
                response_deserializer=statistics__pb2.StatisticDetailTransactionResponse.FromString,
                )
        self.UpdateOperation = channel.unary_unary(
                '/protobuffs.StatisticService/UpdateOperation',
                request_serializer=statistics__pb2.StatisticOperationUpdateRequest.SerializeToString,
                response_deserializer=statistics__pb2.StatisticDetailOperationResponse.FromString,
                )


class StatisticServiceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def CreateOperation(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreateTransaction(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UpdateOperation(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_StatisticServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'CreateOperation': grpc.unary_unary_rpc_method_handler(
                    servicer.CreateOperation,
                    request_deserializer=statistics__pb2.StatisticRequest.FromString,
                    response_serializer=statistics__pb2.StatisticDetailOperationResponse.SerializeToString,
            ),
            'CreateTransaction': grpc.unary_unary_rpc_method_handler(
                    servicer.CreateTransaction,
                    request_deserializer=statistics__pb2.StatisticTransactionCreateRequest.FromString,
                    response_serializer=statistics__pb2.StatisticDetailTransactionResponse.SerializeToString,
            ),
            'UpdateOperation': grpc.unary_unary_rpc_method_handler(
                    servicer.UpdateOperation,
                    request_deserializer=statistics__pb2.StatisticOperationUpdateRequest.FromString,
                    response_serializer=statistics__pb2.StatisticDetailOperationResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'protobuffs.StatisticService', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class StatisticService(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def CreateOperation(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/protobuffs.StatisticService/CreateOperation',
            statistics__pb2.StatisticRequest.SerializeToString,
            statistics__pb2.StatisticDetailOperationResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreateTransaction(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/protobuffs.StatisticService/CreateTransaction',
            statistics__pb2.StatisticTransactionCreateRequest.SerializeToString,
            statistics__pb2.StatisticDetailTransactionResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UpdateOperation(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/protobuffs.StatisticService/UpdateOperation',
            statistics__pb2.StatisticOperationUpdateRequest.SerializeToString,
            statistics__pb2.StatisticDetailOperationResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

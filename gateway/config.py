import os
from dataclasses import dataclass

from dotenv import load_dotenv
load_dotenv()


@dataclass(frozen=True)
class Config:
    # App config
    APP_NAME: str = os.getenv("APP_NAME", 'SAMPLE')
    SWAGGER_PATH: str = os.getenv("SWAGGER_PATH", "/docs")

    # DB config
    DB_HOST: str = os.getenv("DB_HOST", "127.0.0.1")
    DB_PORT: int = os.getenv("DB_PORT", 5432)
    DB_USER: str = os.getenv("DB_USER", 'postgres')
    DB_PASSWORD: str = os.getenv("DB_PASSWORD", 'postgres')
    DB_NAME: str = os.getenv("DB_NAME", 'postgres')
    DATABASE_URL: str = f'postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'  # noqa

    # Auth
    AUTH_SECRET: str = os.getenv('AUTH_SECRET', "Secret")
    TOKEN_EXPIRE_TIME: int = int(os.getenv('TOKEN_EXPIRE_TIME', 3600))

    # Statistic service
    STATISTIC_SERVICE_URL: str = os.getenv('STATISTIC_SERVICE_URL', '127.0.0.1:8888')  # noqa

import grpc
from config import Config
from protobuffs.statistics_pb2 import (
    StatisticRequest,
    StatisticOperationUpdateRequest,
    StatisticTransactionCreateRequest
)
from protobuffs.statistics_pb2_grpc import StatisticServiceStub


class StatisticServiceConnector:
    def __init__(self):
        channel = grpc.insecure_channel(Config.STATISTIC_SERVICE_URL)
        self.client = StatisticServiceStub(channel)

    def create_operation(self) -> dict:
        request = StatisticRequest()
        response = self.client.CreateOperation(request)
        return {"id": response.id, "status": response.status}

    def update_operation(self, operation_id: int, status: int) -> dict:
        request = StatisticOperationUpdateRequest(
            status=status, id=operation_id
        )
        response = self.client.UpdateOperation(request)
        return {"id": response.id, "status": response.status}

    def create_transaction(self, value: int) -> dict:
        request = StatisticTransactionCreateRequest(value=value)
        response = self.client.CreateTransaction(request)
        return {"id": response.id, "value": response.value}

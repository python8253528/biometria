from fastapi import FastAPI

from config import Config
from db import create_db_and_tables
from routes import router


def create_app() -> FastAPI:
    application: FastAPI = FastAPI(
        title=Config.APP_NAME,
        docs_url=Config.SWAGGER_PATH,
    )

    application.include_router(router)
    return application


app: FastAPI = create_app()


# Avoid alembic
@app.on_event("startup")
async def on_startup():
    await create_db_and_tables()

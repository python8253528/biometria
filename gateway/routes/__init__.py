from auth.auth import auth_backend, fastapi_users
from auth.schemas import UserRead, UserCreate
from fastapi import APIRouter
from routes.statistics import router as statistic_router

router = APIRouter()

router.include_router(
    statistic_router,
    prefix='/statistic',
    tags=['Statistics']
)

router.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"]
)
router.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)

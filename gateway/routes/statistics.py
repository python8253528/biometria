from auth.auth import current_active_user
from auth.models import User
from fastapi import APIRouter, Depends
from fastapi import status as statuses
from fastapi.responses import JSONResponse
from service_connectors.statisctics_service import StatisticServiceConnector

router = APIRouter()


@router.post('/operation/create')
def create_operation(user: User = Depends(current_active_user)):
    data = StatisticServiceConnector().create_operation()
    return JSONResponse(content=data, status_code=statuses.HTTP_201_CREATED)


@router.post('/transaction/create')
def create_transaction(value: int, user: User = Depends(current_active_user)):
    data = StatisticServiceConnector().create_transaction(value=value)
    return JSONResponse(content=data, status_code=statuses.HTTP_201_CREATED)


@router.post('/operation/update')
def update_operation_status(
        operation_id: int,
        status: int,
        user: User = Depends(current_active_user)
):
    data = StatisticServiceConnector().update_operation(
        operation_id=operation_id, status=status
    )
    return JSONResponse(content=data, status_code=statuses.HTTP_200_OK)

# Биометрия
## ТЗ:
https://docs.google.com/document/d/15hTT8lOfkIexUagRC2pHMWTrAO9HfGTOA6VeJ5wuJn0/edit
## Запуск
1. Заполнить .env файл в `gateway` и `statistic`
1. ```docker-compose build```
2. ```docker-compose up```

## Документация доступна по ссылке ```127.0.0.1:8080/docs```
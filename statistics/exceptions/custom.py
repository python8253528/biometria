import grpc


class GRPCCustomException(Exception):
    def __init__(self, code: grpc.StatusCode, detail: str = ''):
        self.code = code
        self.detail = detail

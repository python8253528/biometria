import asyncio
from concurrent import futures

import grpc

from config import Config
from db import create_db
from protobuffs import statistics_pb2_grpc
from service import Service


async def serve() -> None:
    port = str(Config.APP_PORT)
    server = grpc.aio.server(futures.ThreadPoolExecutor(max_workers=10))
    statistics_pb2_grpc.add_StatisticServiceServicer_to_server(
        Service(), server
    )
    server.add_insecure_port("[::]:" + port)
    await server.start()
    await server.wait_for_termination()


if __name__ == "__main__":
    create_db()
    # logging.basicConfig()
    asyncio.run(serve())

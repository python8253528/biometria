from enum import Enum


class OperationStatusChoice(Enum):
    Started = 1
    Aborted = 2
    Error = 3
    Done = 4

    @classmethod
    def statuses(cls) -> list:
        return list(map(lambda c: c.value, cls))

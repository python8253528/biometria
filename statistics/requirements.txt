aiosqlite==0.19.0
greenlet==2.0.2
grpcio==1.57.0
grpcio-tools==1.57.0
protobuf==4.24.0
psycopg2-binary==2.9.7
python-dotenv==1.0.0
SQLAlchemy==2.0.19
typing_extensions==4.7.1

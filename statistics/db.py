from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import Config
from models.models import Base

DATABASE_URL = Config.DATABASE_URL

engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine, expire_on_commit=False
)


def create_db() -> None:
    Base.metadata.create_all(engine)

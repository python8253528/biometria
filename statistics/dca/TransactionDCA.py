from db import SessionLocal
from models.models import Transaction


class TransactionDCA:
    def create_transaction(self, value: int = None) -> Transaction:
        with SessionLocal() as session:
            transaction = Transaction(value=value)
            session.add(transaction)
            session.commit()
            return transaction

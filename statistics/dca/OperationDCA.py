from typing import Union

import grpc

from choices import OperationStatusChoice
from db import SessionLocal
from exceptions.custom import GRPCCustomException
from models.models import Operation


class OperationDCA:
    def create_operation(self) -> Operation:
        with SessionLocal() as session:
            op = Operation(status=OperationStatusChoice.Started.value)
            session.add(op)
            session.commit()
            return op

    def update_operation(
            self, operation_id, status
    ) -> Union[Operation, GRPCCustomException]:
        with SessionLocal() as session:
            if status not in OperationStatusChoice.statuses():
                return GRPCCustomException(
                    code=grpc.StatusCode.INVALID_ARGUMENT.value,
                    detail='status'
                )

            op = session.query(Operation).where(
                Operation.id == operation_id
            ).one()

            if not op:
                return GRPCCustomException(code=grpc.StatusCode.NOT_FOUND)

            op.status = status
            session.commit()
            return op

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class StatisticRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class StatisticOperationUpdateRequest(_message.Message):
    __slots__ = ["id", "status"]
    ID_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    id: int
    status: int
    def __init__(self, id: _Optional[int] = ..., status: _Optional[int] = ...) -> None: ...

class StatisticTransactionCreateRequest(_message.Message):
    __slots__ = ["value"]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    value: int
    def __init__(self, value: _Optional[int] = ...) -> None: ...

class StatisticDetailOperationResponse(_message.Message):
    __slots__ = ["id", "status"]
    ID_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    id: int
    status: int
    def __init__(self, id: _Optional[int] = ..., status: _Optional[int] = ...) -> None: ...

class StatisticDetailTransactionResponse(_message.Message):
    __slots__ = ["id", "value"]
    ID_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    id: int
    value: int
    def __init__(self, id: _Optional[int] = ..., value: _Optional[int] = ...) -> None: ...

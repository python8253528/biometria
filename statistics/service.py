from dca.OperationDCA import OperationDCA
from dca.TransactionDCA import TransactionDCA
from exceptions.custom import GRPCCustomException
from protobuffs.statistics_pb2 import (
    StatisticDetailOperationResponse,
    StatisticDetailTransactionResponse
)
from protobuffs.statistics_pb2_grpc import StatisticServiceServicer


class Service(StatisticServiceServicer):
    def CreateOperation(
            self, request, context
    ) -> StatisticDetailOperationResponse:
        result = OperationDCA().create_operation()
        return StatisticDetailOperationResponse(
            id=result.id, status=result.status
        )

    def CreateTransaction(
            self, request, context
    ) -> StatisticDetailTransactionResponse:
        transaction = TransactionDCA().create_transaction(value=request.value)
        return StatisticDetailTransactionResponse(
            id=transaction.id, value=transaction.value
        )

    def UpdateOperation(
            self, request, context
    ) -> StatisticDetailOperationResponse:
        result = OperationDCA().update_operation(
            operation_id=request.id, status=request.status
        )
        if isinstance(result, GRPCCustomException):
            context.set_code(result.code)
            context.set_details(result.detail)
            return StatisticDetailOperationResponse()
        return StatisticDetailOperationResponse(
            id=result.id, status=result.status
        )

from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base

from choices import OperationStatusChoice

Base: DeclarativeMeta = declarative_base()


class Operation(Base):
    __tablename__ = 'operations'

    id = Column(Integer, primary_key=True)
    status = Column(
        Integer, default=OperationStatusChoice.Started.value, nullable=False
    )


class Transaction(Base):
    __tablename__ = "transactions"

    id = Column(Integer, primary_key=True)
    value = Column(Integer, default=0, nullable=False)

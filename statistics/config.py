import os
from dataclasses import dataclass

from dotenv import load_dotenv

load_dotenv()


@dataclass(frozen=True)
class Config:
    # App confi
    APP_PORT: int = os.getenv("APP_PORT", 8888)

    # DB config
    DB_HOST: str = os.getenv("DB_HOST", "127.0.0.1")
    DB_PORT: int = os.getenv("DB_PORT", 5433)
    DB_USER: str = os.getenv("DB_USER", 'postgres')
    DB_PASSWORD: str = os.getenv("DB_PASSWORD", 'postgres')
    DB_NAME: str = os.getenv("DB_NAME", 'postgres2')
    DATABASE_URL: str = f'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'  # noqa
